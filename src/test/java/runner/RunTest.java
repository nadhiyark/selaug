package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@CucumberOptions(
			features="src/test/java/featuresFiles/CreateLead.feature",
			glue= {"tests","pages"},
			monochrome= true,
					//tags= {"@Positive"}
					tags= {"@Positive or @Negative"}
			//tags= {"@positive and @Negative"}
			//dryRun= false,
			//snippets = SnippetType.CAMELCASE
			)
			
	@RunWith(Cucumber.class)		
	public class RunTest {
	}



