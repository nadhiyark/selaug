Feature: Creating a Lead
#
#Background:
#Given Open the Chrome Browser
#And Maximize the Browser
#And Set Timeouts
#And Hit the URL


@Positive
Scenario Outline:  Positive  flow for creating a lead
And Enter the User Name as <uName>
And Enter the Password as <password>
And click on the Login button
And click on CRMSFA link
And click on Leads
And Click on CreateLead
And Enter Company Name as <cName>
And Enter First Name as <fName>
And Enter Last Name as <lName>
When Click on CreateLead button
Then Lead Created Successfully as <expectedText>
  
 
 

Examples:

|uName|password|cName|fName|lName|expectedText|
|DemoSalesManager|crmsfa|Capgemini|Nadhiya|Kumaraswamy|Nadhiya|
|DemoSalesManager|crmsfa|MicroSoft|Nadhiya|Kumaraswamy|Nadhiya|

 
 @Negative
 Scenario: Negative flow for creating a lead
 And Enter the User Name as DemoSalesManager
And Enter the Password as crmsfa
And click on the Login button
And click on CRMSFA link
And click on Leads
And Click on CreateLead

And Enter Company Name as CapGemini
And Enter First Name as Nadhi
And Enter Last Name as Kumaraswamy
When Click on CreateLead button
Then Lead Created Successfully as Nadhiya 
 
  

 
 