/*package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	//private static final TimeUnit TimeUnit = null;
	public ChromeDriver driver;
	
	@Given("Open the Chrome Browser")
	public void OpenBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
		
	}
	
	@And("Maximize the Browser")
	public void maxBrowser() {
		driver.manage().window().maximize();
	}
	
	@And("Set Timeouts")
	public void setTimeOuts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@And("Hit the URL")
	public void OpenTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}
	
	@And("Enter the User Name as (.*)")
	public void enterUserName(String username) {
		driver.findElementById("username").sendKeys(username);
	}
	
	@And("Enter the Password as (.*)")
	public void enterPassword(String password) {
		driver.findElementById("password").sendKeys(password);
	}

	@And("click on the Login button")
	public void clickLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@And("click on CRMSFA link")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@And("click on Leads")
	public void clickLeads() {
		driver.findElementByXPath("//a[text()='Leads']").click();
	}
	
	@And("Click on CreateLead")
	public void clickCreateLeads() {
		driver.findElementByXPath("//a[text()=\"Create Lead\"]").click();
	}

	@And("Enter Company Name as (.*)")
	public void enterCompanyName(String cName) {
		driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys(cName);
	}
	
	@And("Enter First Name as (.*)")
	public void enterFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
	}

	@And("Enter Last Name as (.*)")
	public void enteLastName(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}
	
	@When("Click on CreateLead button")
	public void clickCreateLeadButton() {
		driver.findElementByXPath("//input[@name=\"submitButton\"]").click();
	}
	
	@Then("Lead Created Successfully")
	public void suceess() {
		System.out.println("Lead Creted Successfully");
	}
	
	@But("Lead creation failed")
	public void fail() {
		System.out.println("Lead Creation failed");
	}
	




}
*/