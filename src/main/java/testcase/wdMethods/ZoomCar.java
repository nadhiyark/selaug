package testcase.wdMethods;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {
			public static void main(String[] args) throws InterruptedException {
				
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				
				ChromeDriver driver=new ChromeDriver();
			driver.manage().window().maximize();
			//Implicit wait
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//Loading URL
			driver.get("https://www.zoomcar.com/chennai");
			
			driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
			driver.findElementByXPath("//div[contains(text(),'Thuraipakkam')]").click();
			driver.findElementByXPath("//button[contains(text(),'Next')]").click();
			WebElement Tomorrowdate = driver.findElementByXPath("(//div[@class='days']/div)[2]");
			String date1 = Tomorrowdate.getText();
			System.out.println("date1  "+Tomorrowdate.getText());
			System.out.println("****");
			Tomorrowdate.click();
			driver.findElementByXPath("//button[text()='Next']").click();
			Thread.sleep(3000);
			WebElement checkdate = driver.findElementByXPath("(//div[@class='days']/div)[1]");
			String date2 = checkdate.getText();
			System.out.println("date2  "+checkdate.getText());
			System.out.println("****");
			if(date1.equals(date2)){
				System.out.println("Selection confirmed");
				
			}
			
			driver.findElementByXPath("//button[text()='Done']").click();
			String xpath="//div[@class='car-item']";
			List<WebElement> results = driver.findElementsByXPath(xpath);
			int NoOfResults = results.size();
			System.out.println("No of Results found  "+NoOfResults);
			
			//collections.sort(results);
			//driver.findElementByXPath("//div[@class='item active-input']").click();
			Thread.sleep(5000);
			List<WebElement> highPrice = driver.findElementsByXPath("//div[@class='price']");
			List<Integer> highPricevalue =new ArrayList<Integer> ();
			for (WebElement webElement : highPrice) {
				String substring = webElement.getText().substring(2); 
				highPricevalue.add(Integer.parseInt(substring));
			}
			
			Collections.sort(highPricevalue);
			int sizeofHighVal = highPricevalue.size();
			Integer maxval = highPricevalue.get(sizeofHighVal-1);
			String carname = driver.findElementByXPath("(//div[contains(text(),'"+maxval+"')]/parent::div/parent::div/preceding-sibling::div)[2]/h3").getText();
			
			System.out.println("The maximum price is "+maxval+" and the car name is "+carname);
			
			//div[contains(text(),'520')]
			driver.findElementByXPath("//div[contains(text(),'"+maxval+"')]/following-sibling::button").click();
			driver.quit();
			//driver.quit();
			
			}
	}