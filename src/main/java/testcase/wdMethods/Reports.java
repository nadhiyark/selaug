package testcase.wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		ExtentHtmlReporter html= new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//testcase level
		ExtentTest test = extent.createTest("TestCase001", "Login to the page");
		
		test.assignCategory("functional");
		test.assignAuthor("Nadhiya");
		test.pass("Browser launched successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("the data demosalesmanager entered successfully ", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("The data crmsfa entered successfully ", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("login success");
		
		extent.flush();
	}

}
