package testcase.wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DuplicateLead extends ProjectMethods {
	@Test(groups= {"regression"})
	public void duplicatelead() {
		/*startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement usernameid = locateElement("username");
		type(usernameid, "DemoSalesManager");

		WebElement passwordid = locateElement("password");
		type(passwordid, "crmsfa");

		WebElement submit = locateElement("class", "decorativeSubmit");
		click(submit);

		WebElement crmsfa = locateElement("linkText", "CRM/SFA");
		click(crmsfa);*/
		WebElement leads = locateElement("linkText", "Leads");
		click(leads);

		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);
		WebElement email = locateElement("xpath", "//span[text()='Email']");
		click(email);

		WebElement emailaddress = locateElement("xpath", "//input[@name='emailAddress']");
		type(emailaddress, "nadhiya@gmail.com");

		WebElement finleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(finleads);

		WebElement firstelename = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		String firstelementtext = getText(firstelename);
		click(firstelename);

		WebElement duplicatelead = locateElement("linkText", "Duplicate Lead");
		click(duplicatelead);

		verifyTitle("Duplicate Lead");
		
		WebElement creatlead = locateElement("class", "smallSubmit");
		click(creatlead);
		
		WebElement dupname = locateElement("viewLead_firstName_sp");
		verifyPartialText(dupname, firstelementtext);
		

	}


}
