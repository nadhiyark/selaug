package testcase.wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;



public class DeleteLeads extends ProjectMethods{
	
	//@Test(enabled=false)
	
	@Test(groups={"sanity"})

	public void delete()
	{

		/*startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement usernameid = locateElement("username");
		type(usernameid, "DemoSalesManager");

		WebElement passwordid = locateElement("password");
		type(passwordid, "crmsfa");

		WebElement submit = locateElement("class", "decorativeSubmit");
		click(submit);

		WebElement crmsfa = locateElement("linkText", "CRM/SFA");
		click(crmsfa);*/

		WebElement leads = locateElement("linkText", "Leads");
		click(leads);

		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);

		WebElement phone = locateElement("xpath", "//span[text()='Phone']");
		click(phone);

		WebElement phonenumber = locateElement("xpath", "//input[@name='phoneNumber']");
		type(phonenumber, "9573259657");

		WebElement finleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(finleads);


		WebElement firstrow = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String cleadid = getText(firstrow);
		//System.out.println(cleadid);
		click(firstrow);

		WebElement delete = locateElement("xpath", "//a[@class='subMenuButtonDangerous']");
		click(delete);

		findleads = locateElement("linkText", "Find Leads");
		click(findleads);
		WebElement leaid = locateElement("xpath","(//label[text()='Lead ID:']/following::input)[1]");
		type(leaid, cleadid);
		
		
		 finleads = locateElement("xpath", "//button[text()='Find Leads']");
			click(finleads);


		WebElement error = locateElement("xpath", "//div[text()='No records to display']");
		
		
		verifyExactText(error, "No records to display");
		
		

	}
}
