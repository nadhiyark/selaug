package testcase.wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

//import week4.day1.DragAndDrop;

public class SeMethodsByMe implements WdMethods{

	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver  = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver  = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" launched successfully");
			takeSnap();
		} catch (WebDriverException e) {

			System.err.println("WebDriverException has occured");
			//throw new RuntimeException("WebDriverException has occured");
		}

		catch (NullPointerException e) {

			System.err.println("NullPointerException has occured");
			throw new RuntimeException("NullPointerException has occured");
		}
		catch (Exception e) {

			System.err.println("Exception has occured");
			throw new RuntimeException("Exception has occured");
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);	
			case "name": return driver.findElementByName(locValue);
			case "partiallinktext": return driver.findElementByPartialLinkText(locValue);
			case "tagname": return driver.findElementByTagName(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occured");
			throw new RuntimeException("NoSuchElementException has occured");
		}
		 catch (NotFoundException e) {
				System.err.println("NotFoundException has occured");
				throw new RuntimeException("NotFoundException has occured");
			}
		catch (WebDriverException e) {

			System.err.println("WebDriverException has occured");
			//throw new RuntimeException("WebDriverException has occured");
		}
		catch (Exception e) {

			System.err.println("Exception has occured");
			throw new RuntimeException("Exception has occured");
		}
		return null;
	}

	//finding element by id alone
	public WebElement locateElement(String locValue) {		
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered Successfully");
		takeSnap();
	}


	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}


	public String getText(WebElement ele) {		
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {

		Select drop = new Select(ele);
		drop.selectByVisibleText(value);	

		/*List<WebElement> alloptions = drop.getOptions();
		for (WebElement eachoption : alloptions) {

			if(eachoption.equals(value))
			System.out.println(eachoption.getText());
		}*/
		takeSnap();

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select drop = new Select(ele);
		drop.selectByIndex(index);
		takeSnap();	
	}
	
	public void selectDropdown(WebElement ele,String selectMethod, String value)
	{
		Select dd = new Select(ele);
		try {
			if(selectMethod.equalsIgnoreCase("visible"))
			{
				dd.selectByVisibleText(value);
			}
			else
			 if(selectMethod.equalsIgnoreCase("value"))
			{
				dd.selectByValue(value);
				
			}else
				if(selectMethod.equalsIgnoreCase("index"))
				{
					dd.selectByIndex(Integer.parseInt(value));
				}
			
			System.out.println("dropdown is selected by"+value);
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			System.out.println("The element "+ele+"Couldn't be found");
		}
				
		
			takeSnap();	
	}
	public String getTitle() {		
		String title = "";
		try {
			title =  driver.getTitle();
		} catch (WebDriverException e) {
			System.out.println("WebDriverException"+e.getMessage());
		} 
		return title;
	}


	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean bReturn =false;
		try {
			if(getTitle().equals(expectedTitle)) {
				System.out.println("The expected title "+expectedTitle+" matches the actual "+getTitle());
				bReturn= true;
			}else {
				System.out.println(getTitle()+" The expected title "+expectedTitle+" doesn't matches the actual "+getTitle());
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
		return bReturn;




	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
		if(ele.getText().equals(expectedText))
		{
			System.out.println(ele.getText()+"is same as expected");
		}
		
		else
		{
			System.out.println(ele.getText()+"is not an epected text");
		}
		
		takeSnap();

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
		if(ele.getText().contains(expectedText))
		{
			System.out.println(ele.getText()+ "is same as expected");
		}
		else
		{
			System.out.println("actual is not same as expected");
		}
			

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		
		Set<String> allwindows = driver.getWindowHandles();
		List<String> listofwindows= new ArrayList<>();
		listofwindows.addAll(allwindows);
		
		driver.switchTo().window(listofwindows.get(index));
				

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");		
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub

	}
	
	public void clear(WebElement ele)
	{
		ele.clear();
	}

}
