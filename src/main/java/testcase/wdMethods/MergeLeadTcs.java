package testcase.wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class MergeLeadTcs extends SeMethods {
	@Test
	public void mergeLead()  {
		startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement usernameid = locateElement("username");
		type(usernameid, "DemoSalesManager");

		WebElement passwordid = locateElement("password");
		type(passwordid, "crmsfa");

		WebElement submit = locateElement("class", "decorativeSubmit");
		click(submit);


		WebElement crmsfa = locateElement("linkText", "CRM/SFA");
		click(crmsfa);

		WebElement leads = locateElement("linkText", "Leads");
		click(leads);

		WebElement mergeleads = locateElement("linkText", "Merge Leads");
		click(mergeleads);

		WebElement icon1 = locateElement("xpath", "//table[@name='ComboBox_partyIdFrom']/following::img");
		click(icon1);

		switchToWindow(1);

		WebElement leadid = locateElement("xpath", "//input[@name='id']");
		type(leadid, "1");

		WebElement finleads = locateElement("xpath", "//button[@class='x-btn-text']");
		click(finleads);



		WebElement firstele = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String FromLeadId = getText(firstele);
		click(firstele);

		switchToWindow(0);

		WebElement icon2 = locateElement("xpath", "//table[@name='ComboBox_partyIdTo']/following::img");
		click(icon2);

		switchToWindow(1);

		WebElement leadid1 = locateElement("xpath", "//input[@name='id']");
		type(leadid, "1");

		WebElement finleads1 = locateElement("xpath", "//button[@class='x-btn-text']");
		click(finleads);

		WebElement firstele1 = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[2]");

		click(firstele1);
		switchToWindow(0);

		WebElement merge = locateElement("class", "buttonDangerous");
		click(merge);

		acceptAlert();
		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);

		WebElement ledid = locateElement("xpath", "(//div[@class='x-form-element']/following::input)[32]");

		type(ledid, FromLeadId);

		WebElement findledbutton = locateElement("xpath", "(//td[@class='x-panel-btn-td'])[6]");
		click(findledbutton);
		
		WebElement error = locateElement("xpath", "//div[text()='No records to display']");
		
		verifyPartialText(error, "No records to display");




	}




}
