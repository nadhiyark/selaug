package testcase.wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class EditLead extends SeMethods {
	@Test(dependsOnMethods= {"testcase.wdMethods.CreateLead.createLead"})

	public void edit  () throws InterruptedException
	{

		startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement usernameid = locateElement("username");
		type(usernameid, "DemoSalesManager");

		WebElement passwordid = locateElement("password");
		type(passwordid, "crmsfa");

		WebElement submit = locateElement("class", "decorativeSubmit");
		click(submit);

		WebElement crmsfa = locateElement("linkText", "CRM/SFA");
		click(crmsfa);

		WebElement leads = locateElement("linkText", "Leads");
		click(leads);

		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);

		WebElement firstname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(firstname, "nadhiya");

		WebElement finleads = locateElement("xpath","//button[text()='Find Leads']");
		click(finleads);
		Thread.sleep(5000);

		WebElement firstele = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		click(firstele);
		boolean verifyTitle = verifyTitle("View Lead | opentaps CRM");
		System.out.println(verifyTitle);

		WebElement edit = locateElement("xpath","(//a[@class='subMenuButton'])[3]");
		click(edit);

		
		WebElement changecomp = locateElement("id", "updateLeadForm_companyName");
		
		//clear(changecomp);
		type(changecomp, "Igate");

		WebElement upadte = locateElement("xpath", "(//input[@name='submitButton'])[1]");
		click(upadte);
	
		WebElement verifycomp = locateElement("viewLead_companyName_sp");
		verifyPartialText(verifycomp, "Igate");
		
		closeBrowser();

	}
}
