package week1.day2;

import java.util.Scanner;

public class HomeWork1 {

	public static void main(String[] args) {
		// Take 5 nums as i/p in an array and fin out the maximum number
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter the no.of element in the array: ");
		int n= scan.nextInt();
		
		int numbers[]= new int[n];
		
		System.out.println("Enter the element in the array: ");
				
		for(int i=0; i<n;i++) {
			numbers[i]=scan.nextInt();
			
		}
		
		int largest=numbers[0];
		for(int i=1;i<n;i++)
		{
			if(largest<numbers[i])
			{
				largest=numbers[i];
			}
		}
		
		System.out.println("Greatest number is :" +largest);

	}

}
