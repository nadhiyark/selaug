package week1.day2;

import java.util.Scanner;

public class TuesdayTask1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
// Write a program to get a number the console and print its factorial
		
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter a number");
		int number = scan.nextInt();
		
		int factorial =1;
		
		for(int i=1; i<=number; i++)
		{
			factorial = factorial*i;
		}
		System.out.println(factorial);
	}

}
