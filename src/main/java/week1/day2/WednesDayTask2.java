package week1.day2;

import java.util.Scanner;

public class WednesDayTask2 {

	public void getNumber(int number, int year) {

		int days=0;
		String month=null;

		switch (number)
		{
		case 1: 
			month= "Jan";
			days = 31;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;

		case 2: 
			month= "feb";

			if((year%400==0)||((year%4==0)))
			{
				if(year%100!=0) {
					days=29;
				}
			}
			else
				days=28;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;

		case 3: 
			month= "March";
			days = 31;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;

		case 4: 
			month= "April";
			days = 30;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
		case 5: 
			month= "May";
			days = 31;
			
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
		case 6: 
			month= "June";
			days = 30;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
		case 7: 
			month= "July";
			days = 31;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
			
		case 8: 
			month= "Aug";
			days = 31;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
			
		case 9: 
			month= "Sep";
			days = 30;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
			
		case 10: 
			month= "oct";
			days = 31;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
		case 11: 
			month= "Nov";
			days = 30;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;
		case 12: 
			month= "Dec";
			days = 31;
			System.out.println("the "+ number+"st month of the year is "+month);
			System.out.println("Number of days in the month of"+month+ "is" +days);
			break;

		default:
			System.out.println("Invalid month ");
		}


	}


	public static void main(String[] args) {
		/* Write a Java program to determine the month based on the given number and 
		find the no.of days in that month  */

		Scanner scan =new Scanner(System.in);
		System.out.println("Enter the number");
		int number = scan.nextInt();
		System.out.print("Enter a year: ");
		int year=scan.nextInt();

		WednesDayTask2 obj = new WednesDayTask2();
		obj.getNumber( number, year);



	}
}


