package week1.day1;

import java.util.Scanner;

public class HomeWork1 {

	public static void main(String[] args) {
		// store five values or integers in the array and sort it

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the range");

		int n= scan.nextInt();

		int arr[]= new int[n];

		System.out.println("Enter the values in the array");

		for(int i=0; i<n; i++)
		{
			arr[i]=scan.nextInt();
		}

		System.out.println("Values in the array");

		
		int temp=0;
		for(int i=0; i<n; i++) {
			for(int j=i+1; j<n-1; j++)
			{
				if(arr[i]>arr[j])
				{
				temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
		}
		
		System.out.println("Print the sorted array ");
		for(int i=0; i<n; i++)
		{
			System.out.println(arr[i]);
		}
	}

}
