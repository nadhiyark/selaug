package week1.day1;

import java.util.Scanner;

public class SumOfOddNumberInTheGivenDigit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a number =");
		int number= scan.nextInt();
		
		int odd_digit_sum=0;
		while(number>0)
		{
			int digit = number%10;
			
			if(digit%2 !=0)
			{
				odd_digit_sum=odd_digit_sum+digit;
			}
			number= number/10;
					}
		System.out.println(odd_digit_sum);
		
		//System.out.println(number);
		
	}

}
