package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import tests.Hooks;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

//public class CreateLeadPage extends ProjectMethods {
	public class CreateLeadPage extends SeMethods {
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(xpath="(//input[@name='companyName'])[2]")
	
	WebElement eleCName;
	@And("Enter Company Name as (.*)")
	public CreateLeadPage enterCompanyName(String cName) {
		
		type(eleCName, cName);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_firstName")
	WebElement eleFName;
	@And("Enter First Name as (.*)")
	public CreateLeadPage enterFName(String fName) {
		
		type(eleFName, fName);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_lastName")
	WebElement eleLName;
	@And("Enter Last Name as (.*)")
	public CreateLeadPage enterLName(String lName) {
		
		type(eleLName, lName);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_firstNameLocal")
	WebElement eleFirstNameLocal;
	public CreateLeadPage enterFirstNameLocal(String fNameLocal) {
		
		type(eleFirstNameLocal, fNameLocal);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_lastNameLocal")
	WebElement eleLastNameLocal;
	public CreateLeadPage enterLastNameLocal(String lNameLocal) {
		
		type(eleLastNameLocal, lNameLocal);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_personalTitle")
	WebElement eleSalutation;
	public CreateLeadPage enterSalutation(String salutation) {
		
		type(eleSalutation, salutation);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_dataSourceId")
	WebElement eleSource;
	public CreateLeadPage enterSource(String source) {
		
		type(eleSource, source);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalProfTitle")
	WebElement eleTitle;
	public CreateLeadPage enterTitle(String title) {
		
		type(eleTitle, title);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_annualRevenue")
	WebElement eleRevenue;
	public CreateLeadPage enterAnnualRevenue(String revenue) {
		
		type(eleTitle, revenue);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_industryEnumId")
	WebElement eleIndustry;
	public CreateLeadPage selectIndustry(String visibletext) {
		
		selectDropdown(eleIndustry, "visible", visibletext);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_ownershipEnumId")
	WebElement eleOwnership;
	public CreateLeadPage selectOwnership(String value) {
		
		selectDropdown(eleOwnership, "value", value);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_sicCode")
	WebElement eleSicCode;
	public CreateLeadPage enterSicCode(String sicCode) {
		
		type(eleSicCode, sicCode);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_description")
	WebElement eleDescription;
	public CreateLeadPage enterDescription(String description) {
		
		type(eleDescription, description);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_importantNote")
	WebElement eleImpNote;
	public CreateLeadPage enterImportantNote(String impNote) {
		
		type(eleImpNote, impNote);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_primaryPhoneCountryCode")
	WebElement eleCountryCode;
	public CreateLeadPage enterCountryCode(String countrycode) {
		
		type(eleCountryCode, countrycode);
		return this;
		
	}
	

	@FindBy(id="createLeadForm_primaryPhoneExtension")
	WebElement eleExtension;
	public CreateLeadPage enterExtension(String extension) {
		
		type(eleExtension, extension);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_departmentName")
	WebElement eleDepartmentName;
	public CreateLeadPage enterDepartmentName(String deptName) {
		
		type(eleDepartmentName, deptName);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_currencyUomId")
	WebElement eleCurrency;
	public CreateLeadPage enterCurrency(String currency) {
		
		type(eleCurrency, currency);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_numberEmployees")
	WebElement eleNoOfEmployees;
	public CreateLeadPage enterNoOfEmployees(String noOfEmp) {
		
		type(eleNoOfEmployees, noOfEmp);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_tickerSymbol")
	WebElement eleTickerSymbol;
	public CreateLeadPage enterTickerSymbol(String ticker) {
		
		type(eleTickerSymbol, ticker);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_primaryPhoneAskForName")
	WebElement elePrimaryPhoneAskForName;
	public CreateLeadPage enterPrimaryPhoneAskForName(String person) {
		
		type(elePrimaryPhoneAskForName, person);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_primaryWebUrl")
	WebElement elePrimaryWebUrl;
	public CreateLeadPage enterPrimaryWebUrl(String url) {
		
		type(elePrimaryWebUrl, url);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalToName")
	WebElement eleGeneralToName;
	public CreateLeadPage entergeneralToName(String toName) {
		
		type(eleGeneralToName, toName);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalAddress1")
	WebElement eleGeneralAddress1;
	public CreateLeadPage enterGeneralAddress1(String address1) {
		
		type(eleGeneralAddress1, address1);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalAddress2")
	WebElement eleGeneralAddress2;
	public CreateLeadPage enterGeneralAddress2(String address2) {
		
		type(eleGeneralAddress2, address2);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalCity")
	WebElement eleGeneralCity;
	public CreateLeadPage enterGeneralCity(String city) {
		
		type(eleGeneralCity, city);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalStateProvinceGeoId")
	WebElement eleGeneralStateProvinceGeoId;
	public CreateLeadPage selectGeneralStateProvinceGeoId(String visibleText) {
		
		selectDropdown(eleGeneralStateProvinceGeoId, "visible", visibleText);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalStateProvinceGeoId")
	WebElement eleGeneralCountry;
	public CreateLeadPage selectGeneralCountry(String index) {
		
		selectDropdown(eleGeneralCountry, "index", index);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalPostalCode")
	WebElement elePostalCode;
	public CreateLeadPage enterPostalCode(String postalCode) {
		
		type(elePostalCode, postalCode);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_generalPostalCode")
	WebElement elePostalCodeText;
	public CreateLeadPage enterPostalCodeText(String zipext) {
		
		type(elePostalCodeText, zipext);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_marketingCampaignId")
	WebElement eleMarketingCampaignId;
	public CreateLeadPage enterMarketingCampaignId(String marketingCampaignId) {
		
		type(eleMarketingCampaignId, marketingCampaignId);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_primaryPhoneNumber")
	WebElement elePrimaryPhoneNumber;
	public CreateLeadPage enterPrimaryPhoneNumber(String primaryPhoneNumber) {
		
		type(elePrimaryPhoneNumber, primaryPhoneNumber);
		return this;
		
	}
	
	@FindBy(id="createLeadForm_primaryEmail")
	WebElement elePrimaryEmail;
	public CreateLeadPage enterPrimaryEmail(String primaryEmail) {
		
		type(elePrimaryEmail, primaryEmail);
		return this;
		
	}
	
	
	
	@FindBy(xpath="//input[@name=\"submitButton\"]")
	WebElement eleBClick;
	@When("Click on CreateLead button")
	public ViewLeadPage clickCreateLeadButton() {
		
		click(eleBClick);
		return new ViewLeadPage();
		
	}
}
