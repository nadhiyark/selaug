package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;


	public class OpenTapsCrmPage extends ProjectMethods {
		
		public OpenTapsCrmPage() {
			PageFactory.initElements(driver, this);
		}
		
		@FindBy(how=How.ID, using="updateLeadForm_companyName")
		WebElement eleCompName;
		public OpenTapsCrmPage editCompanyName(String CompName) {
			type(eleCompName, CompName);
			return this;
			
		}
		
		@FindBy(how=How.XPATH, using="(//input[@name='submitButton'])[1]")
		WebElement eleUpadtebtn;
		public ViewLeadPage clickUpdateButton() {
			click(eleUpadtebtn);
			return new ViewLeadPage();
			
		}

}
