package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import tests.Hooks;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

//public class MyLeadspage extends ProjectMethods {
	
	public class MyLeadspage extends SeMethods {
	
	public MyLeadspage() {
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how=How.XPATH,using="//a[text()=\"Create Lead\"]")
	WebElement eleCreateLead;
	
	@And("Click on CreateLead")
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	@FindBy(how=How.LINK_TEXT,using="Find Leads")
	WebElement eleFindLeads;
	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new FindLeadsPage();
	}

	

	

}
