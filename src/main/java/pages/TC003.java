package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003 extends ProjectMethods{
	@BeforeTest(groups={"common"})
	public void setData() {
		testCaseName = "TC003";
		testCaseDesc= "Deletelead";
		category="Smoke";
		author="nadhiya";
		fileName="DeleteLead";
	}
		
	
@Test(dataProvider="positive")
	public void login(String uName, String pass, String pNumber) {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pass)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindLeads()
		.clickPhone()
		.enterPhoneNumber(pNumber)
		.clickFindLeadsbutton()
		.clickFirstResult()
		.clickDelete()
		.clickFindLeads();
		
		
		
		
}
}
