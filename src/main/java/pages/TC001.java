package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC001 extends ProjectMethods {
	
	@BeforeTest(groups={"common"})
	public void setData() {
		testCaseName = "TC001";
		testCaseDesc= "Creating a lead";
		category="Smoke";
		author="nadhiya";
		fileName="CreateLead";
	}
@Test(dataProvider="positive")
	public void login(String uName, String pass, String cName, String fName, String lName, String fNameLocal, 
		String lNameLocal, String salutation, String source, String title, String revenue, String primaryPhoneNumber,
		
		String primaryEmail, String expectedText) {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pass)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFName(fName)
		.enterLName(lName)
		.enterFirstNameLocal(fNameLocal)
		.enterLastNameLocal(lNameLocal)
		.enterSalutation(salutation)
		.enterSource(source)
		.enterTitle(title)
		.enterAnnualRevenue(revenue)
		.enterPrimaryPhoneNumber(primaryPhoneNumber)
		.enterPrimaryEmail(primaryEmail)
		.clickCreateLeadButton()
		.verifyFirstName(expectedText);
		
		
	}
}
