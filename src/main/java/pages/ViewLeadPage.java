package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import tests.Hooks;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

//public class ViewLeadPage extends ProjectMethods {
	public class ViewLeadPage extends SeMethods {
	
	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//input[@name=\"submitButton\"]")
	WebElement eleFName;
	@Then("Lead Created Successfully as (.*)")
	public ViewLeadPage verifyFirstName(String expectedText) {
		verifyExactText(eleFName, expectedText);
		return this;
		
	}
		
	
	@FindBy(how=How.XPATH, using="//input[@name=\"submitButton\"]")
	WebElement eleTitleOfPage;
	public ViewLeadPage verifyTitleOfThePage(String expectedText) {
		verifyExactText(eleTitleOfPage, expectedText);
		return this;
		
	}
	
	@FindBy(how=How.XPATH, using="(//a[@class='subMenuButton'])[3]")
	WebElement eleEdit;
	public OpenTapsCrmPage clickEdit() {
		click(eleEdit);
		return new OpenTapsCrmPage() ;
		
	}
	
	@FindBy(how=How.XPATH, using= "//a[@class='subMenuButtonDangerous']")
	WebElement eleDelete;
	public MyLeadspage clickDelete() {
		click(eleDelete);
		return new MyLeadspage();
		
	}
	
	@FindBy(how=How.LINK_TEXT, using= "linkText")
	WebElement eleDuplicate;
	public MyLeadspage clickDuplicateLead() {
		click(eleDuplicate);
		return new MyLeadspage();
		
	}
	
	@FindBy(how=How.LINK_TEXT, using= "linkText")
	WebElement eleDuplicate1;
	public DuplicateLeadPage verifyTitleDuplicate(String expectedText) {
		click(eleDuplicate1);
		verifyTitleOfThePage(expectedText);
		return new DuplicateLeadPage();
		
	}
	
	


}
