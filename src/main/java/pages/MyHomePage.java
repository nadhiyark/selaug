package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import tests.Hooks;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

//public class MyHomePage extends ProjectMethods {
	public class MyHomePage extends  SeMethods {
	
	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//a[text()='Leads']")
	WebElement elelead;
	@And("click on Leads")
	public MyLeadspage clickLead() {
		click(elelead);
		return new MyLeadspage();
		
	}
	
	

}
