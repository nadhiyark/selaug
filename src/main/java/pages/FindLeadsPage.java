package pages;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]")
	WebElement eleFirstName;
	public FindLeadsPage enterFirstName(String fname) {
		type(eleFirstName, fname);

		return this;
	}

	
	@FindBy(how=How.XPATH,using="//span[text()='Phone']")
	WebElement elePhone;
	public FindLeadsPage clickPhone() {
		click(elePhone);

		return this;
	}
	
	@FindBy(how=How.XPATH,using="//span[text()='Email']")
	WebElement eleEmail;
	public FindLeadsPage clickEmail() {
		click(eleEmail);

		return this;
	}
	
	@FindBy(how=How.XPATH,using="//input[@name='phoneNumber']")
	WebElement elePhoneNumber;
	public FindLeadsPage enterPhoneNumber(String pNumber) {
		type(elePhoneNumber, pNumber);

		return this;
	}
	
	@FindBy(how=How.XPATH,using="//input[@name='emailAddress']")
	WebElement eleEmailAddress;
	public FindLeadsPage enterEmailAddress(String emailID) {
		type(eleEmailAddress, emailID);

		return this;
	}


	
	@FindBy(how=How.XPATH,using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]")
	WebElement eleFirstResult;
	public ViewLeadPage captureLeadId() {
		String LeadID = getText(eleClickFirstResult);
		return new ViewLeadPage();
	}
	
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']")
	WebElement eleFindLeadsbutton;
	public FindLeadsPage clickFindLeadsbutton() {
		click(eleFindLeadsbutton);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
		
	}

	@FindBy(how=How.XPATH,using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]")
	WebElement eleClickFirstResult;
	public ViewLeadPage clickFirstResult() {
		click(eleClickFirstResult);
		return new ViewLeadPage();
	}
	
	@FindBy(how=How.XPATH,using="(//label[text()='Lead ID:']/following::input)[1]")
	WebElement eleLeadId;
	public FindLeadsPage enterCapturedLeadId(String LeadID)
	{
		//this.LeadID=leadID;
		type(eleLeadId, LeadID);
		return  this;
	}
	
	
	
	


}

