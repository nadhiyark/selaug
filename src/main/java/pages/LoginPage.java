package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import tests.Hooks;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

//public class LoginPage extends ProjectMethods {

	public class LoginPage extends SeMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="username")
	//	@FindBy(how=How.ID, using="username")
	WebElement eleUserName;
	@And("Enter the User Name as (.*)")
	public LoginPage typeUserName(String uName) {
		//WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);

		return this;
	}

	@FindBy(id="password")
	WebElement elePassword;
	@And("Enter the Password as (.*)")
	public LoginPage typePassword(String password) {
		//WebElement eleUserName = locateElement("id", "username");
		type(elePassword, password);
		
		return this;
	}


	public LoginPage verifyUserNameColor(String data) {
		verifyExactAttribute(eleUserName, "id", data);
		return this;
	}

	@FindBy(className="decorativeSubmit")
	//@FindBy(how=How.CLASS_NAME, using="decorativeSubmit")
	WebElement eleLogin;
	@And("click on the Login button")
	public HomePage clickLogin() {
		click(eleLogin);

		return new HomePage();
	}

}
