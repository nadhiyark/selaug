package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002 extends ProjectMethods{

	@BeforeTest(groups={"common"})
	public void setData() {
		testCaseName = "TC002";
		testCaseDesc= "editlead";
		category="Smoke";
		author="nadhiya";
		fileName="EditLead";
	}
		
	
@Test(dataProvider="positive")
	public void login(String uName, String pass, String fname, String title, String CompName, String expectedfname) {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pass)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindLeads()
		.enterFirstName(fname)
		.clickFindLeadsbutton()
		.clickFirstResult()
		.verifyTitleOfThePage(title)
		.clickEdit()
		.editCompanyName(CompName)
		.clickUpdateButton()
		.verifyFirstName(expectedfname);
						
		
		
}
}
