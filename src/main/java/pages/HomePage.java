package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import tests.Hooks;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

//public class HomePage extends ProjectMethods {
	
	public class HomePage extends SeMethods {
	
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA")
	WebElement eleCRMSFA;
	@And("click on CRMSFA link")
	public MyHomePage clickCRMSFA() {
		click(eleCRMSFA);
		
		return new MyHomePage();
	}

}
