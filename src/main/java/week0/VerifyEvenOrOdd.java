package week0;

import java.util.Scanner;

public class VerifyEvenOrOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Check the number is even or odd
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter a number: ");
		int number = scan.nextInt();
		
		if(number%2==0)
		{
			System.out.println("Even Number");
		}
		
		else
		{
			System.out.println("Odd Number");
		}

	}

}
