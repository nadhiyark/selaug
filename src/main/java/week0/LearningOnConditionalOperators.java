package week0;

import java.util.Scanner;

public class LearningOnConditionalOperators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter n1 =");
		int n1= scan.nextInt();
		System.out.println(n1);
		System.out.print("Enter n2 =");
		int n2= scan.nextInt();
		System.out.println(n2);

		System.out.println("n1==n2 :"+ (n1==n2));
		System.out.println("n1<n2 :"+ (n1<n2));
		System.out.println("n1<=n2 :"+ (n1<=n2));
		System.out.println("n1>n2 :"+ (n1>n2));
		System.out.println("n1>=n2 :"+ (n1>=n2));
		System.out.println("n1!=n2 :"+ (n1!=n2));

		System.out.print("Enter string n1 =");

		String string1= scan.next();

		System.out.print("Enter string n2 =");

		String string2= scan.next();

		System.out.println("n1==n2 :"+ (string1==string2));
		
		int ar[] = { 1, 2, 3 };
		int br[] = { 1, 2, 3 };
		boolean condition = true;
		// Arrays cannot be compared with
		// relational operators because objects
		// store references not the value
		System.out.println("x == y : " + (ar == br));

		System.out.println("condition==true :" + (condition == true));




	}

}
