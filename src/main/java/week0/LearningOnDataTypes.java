package week0;

public class LearningOnDataTypes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Declaring 'boolean, values are true and false'
		//default value of boolean is false

		boolean a = true;
		boolean b = false;



		System.out.println("boolean value is "+ a);
		System.out.println("boolean value is "+ b);

		//Size: 8 bit, Value: -128 to 127 (inclusive)

		// byte c = -129; can't be a byte

		byte c = -128;
		byte d= 127;
		//byte e= 128;can't be a byte

		System.out.println(c);


		System.out.println(d);

		// It overflows here because
		// byte can hold values from -128 to 

		d++;

		System.out.println(d);
		d++;
		System.out.println(d);


		// @ Short:: Size: 16 bit, Value: -32,768 to 32,767 (inclusive)

		short s = 56;

		// this will give error as number is 
		// larger than short range
		// short s1 = 87878787878;

		System.out.println(s);

		// int ; Size: 32 bit,  Value: -2 ^31 to 2 ^31-1


		// Integer data type is generally 
		// used for numeric values
		int i=89;

		System.out.println(i);


      // @ long Size: 64 bit, 	Value: -2^63 to 2^63-1.
		
		long l = 1234567895555588511l;
		
		System.out.println(l);
		
		 // by default fraction value is double in java
        double doublee = 4.355453532;
        System.out.println(doublee);
         
	
		// @ Float Size: 32 bits, Suffix : F/f Example: 9.8f
		
	    // for float use 'f' as suffix
        float f = 4.7333434f;
        
		System.out.println(f);
        
	}


}