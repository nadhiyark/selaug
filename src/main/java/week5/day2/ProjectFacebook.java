package week5.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ProjectFacebook {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("email").sendKeys("thasleempatan");
		driver.findElementById("pass").sendKeys("18127925");
		driver.findElementByXPath("//input[@type='submit']").click();
		
		driver.findElementByXPath("//input[@name='q']").sendKeys("TestLeaf");
		Thread.sleep(2000);
		driver.findElementByXPath("//button[@type='submit']/i").click();
		
		String text = driver.findElementByXPath("(//button[@type='submit'])[2]").getText();
		System.out.println(text);
		
		if(text.equals("Like"))
		{
			driver.findElementByXPath("(//button[@type='submit'])[2]").click();
		}
		else {
			System.out.println("TestLeaf already liked");
		}
		driver.findElementByXPath("//div[text()='TestLeaf']").click();
		
		boolean b = driver.getTitle().contains("TestLeaf");
		
		if(b==true) {
			System.out.println("Title matched");
		}
		else {
			System.out.println("Title unmatched");
		}
		
		String text2 = driver.findElementByXPath("//div[contains(text(),'people like this')]").getText();
		System.out.println(text2);
		
		driver.close();
	}

}
