package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Sep4Task2 {
	static int i=0;

	public static void main(String[] args) {
		
		
		
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		// Identify the element and store
		WebElement element = driver.findElementById("userRegistrationForm:countries");
		
		// create an object using Select class for an webelement 
		Select dropdown = new Select(element);
		

		// get collection of webelement and store it in list

		List<WebElement> options = dropdown.getOptions();
		
		//iterations through each webElement based on condection
		
		for (WebElement eachoption : options) {
			if(eachoption.getText().startsWith("E"))
			{
				i++;
				if(i==2)
				{
					System.out.println(eachoption.getText());
					eachoption.click();
					break;
				}
					
			}
			
		}
	}

	
}
