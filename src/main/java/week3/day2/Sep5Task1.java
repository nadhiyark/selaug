package week3.day2;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Sep5Task1 {
	
	static int i=0;

	public static void main(String[] args) throws InterruptedException {
		
		
		
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://www.leafground.com/pages/Dropdown.html");
		
		// Identify the element and store
		WebElement element = driver.findElementById("dropdown1");
		
		// create an object using Select class for an webelement 
		Select dropdown1 = new Select(element);
		//dropdown.selectByIndex(2);
		List <WebElement> options= dropdown1.getOptions();
		//dropdown1.selectByIndex(options.size()-1);
		
		int lastoption =options.size()-1;
		for (WebElement eachoption : options) {
			if(i==lastoption)
			{
				eachoption.click();
			}
			i++;
		}
		
		
		
		
		
		
		
	}
}