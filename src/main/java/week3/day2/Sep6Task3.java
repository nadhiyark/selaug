package week3.day2;

import java.util.Scanner;

public class Sep6Task3 {

	public static void main(String[] args) {
		// WAP to print 2nd largest number in the array
		Scanner scan = new Scanner(System.in);
		System.out.println("ENter the size of array");
		int arraysize= scan.nextInt();		
		int a []= new int[arraysize];

		System.out.println("Enter element in the aary");
		for(int i=0; i<arraysize;i++)
		{
			a[i]= scan.nextInt();
		}
		for(int i=0;i<arraysize;i++)
		{
			for(int j=0;j<arraysize-1;j++)
			{
				if(a[j]>a[j+1]){
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
		}
		System.out.println("\n Ascending  array");
		
		for(int i=0;i<arraysize;i++) 
		{

			System.out.println(a[i]);
		}

		System.out.println("Second Largest:"+ (a[arraysize-2]));

	}

}
