package week3.day2;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Sep5Task3 {
	
	static int i=0;

	public static void main(String[] args) throws InterruptedException {
		
		
		
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leafground.com/pages/checkbox.html");

		
		
		WebElement checkbox = driver.findElementByXPath("//input[@type='checkbox']");
		checkbox.click();
		
		if(checkbox.isSelected())
		{
			System.out.println("The check box is selected");
		}
		
		else
			
			System.out.println("The check box is not selected");
			}
		
		
	}
		
		
		
		
	