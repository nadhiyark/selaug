package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Sep1Task1 {


	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		driver.findElementById("userRegistrationForm:userName").sendKeys("test_123");
		driver.findElementById("userRegistrationForm:password").sendKeys("Test_123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Test_123");
		
		WebElement SecurityQuestion= driver.findElementById("userRegistrationForm:securityQ");
		Select secqn= new Select(SecurityQuestion);
		List <WebElement> options= secqn.getOptions();
		secqn.selectByIndex(options.size()-1);
		
		
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Surya");
		
		WebElement preferredlanguage= driver.findElementById("userRegistrationForm:prelan");
		Select language= new Select(preferredlanguage);
		language.selectByIndex(0);
		
		//Personal Details
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Nadhiya");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Ramapuram");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Kumaraswamy");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		//Dob
		WebElement date=driver.findElementById("userRegistrationForm:dobDay");
		Select dobday = new Select(date);
		dobday.selectByIndex(3);
		
		WebElement month =driver.findElementById("userRegistrationForm:dobMonth");
		
		Select dobmonth = new Select(month);
		List <WebElement> options1 =dobmonth.getOptions();
		dobmonth.selectByIndex(options1.size()-1);
		
		WebElement year= driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dobyear = new Select(year);
		
		dobyear.selectByValue("1994");
		
		//Occupation
		WebElement occu = driver.findElementById("userRegistrationForm:occupation");
		Select occupaton= new Select(occu);
		List <WebElement> option2= occupaton.getOptions();
		occupaton.selectByIndex(option2.size()-3);
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("1234567890");
		driver.findElementById("userRegistrationForm:idno").sendKeys("AZPPN12345");
		
		
		WebElement Countries = driver.findElementById("userRegistrationForm:countries");
		Select coun = new Select(Countries);
		coun.selectByIndex(1);
		
		driver.findElementById("userRegistrationForm:email").sendKeys("nadhiya.m4@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("8332046265");
		
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select nation = new Select(nationality);
		nation.selectByIndex(1);
		
		//ResedentialAddress
		
		driver.findElementById("userRegistrationForm:address").sendKeys("34-1015");
		driver.findElementById("userRegistrationForm:street").sendKeys("Bhajana Koil street");
		driver.findElementById("userRegistrationForm:area").sendKeys("Mkbt");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("517127",Keys.TAB);
		
	//Thread.sleep(5000);
		WebElement city= driver.findElementById("userRegistrationForm:cityName");
		Select cityy= new Select(city);
		cityy.selectByIndex(1);
		
		//Thread.sleep(5000);
		WebElement postoffice= driver.findElementById("userRegistrationForm:postofficeName");
		Select post = new Select(postoffice);
		post.selectByValue("Murukambattu S.O");
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("9573259657");
		
		
		
		
		
		
		
		

	}

}
