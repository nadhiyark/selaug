package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Sep6Task1 {

	public static void main(String[] args) {
		
		// Print text of all the options under the dropdown
				System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
				ChromeDriver driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
				driver.manage().window().maximize();
				
				driver.get("http://leafground.com/pages/Dropdown.html");
				
				
				WebElement element= driver.findElementById("dropdown1");
				Select dropdown= new Select(element);
				List<WebElement> options = dropdown.getOptions();
				
				for (WebElement eachoption : options) {
					
					System.out.println(eachoption.getText());
					
				}

	}

}
