package week3.day2;

public class Sep6Task2 {

	public static void main(String[] args) {
		// Swap 2 numbers without using 3 rd variable

		int a= 2;
		int b=3;

		System.out.println("before swaping:"+ " " +a+","+b);
		a=a+b;
		b=a-b;
		a=a-b;

		System.out.println("after swaping:"+ " " +a+","+b);
	}

}
