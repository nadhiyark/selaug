package week3.class1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		try {
			// Set binding
			// maximize the window
			driver.manage().window().maximize();
			// Set timeout
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// load URL
			driver.get("http://leaftaps.com/opentaps");
			// Enter user name
			try {
				driver.findElementById("username1")
				.sendKeys("DemoSalesManager", Keys.TAB);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			// Enter password
			driver.findElementById("password").sendKeys("crmsfa");
			// Click Login
			driver.findElementByClassName("decorativeSubmit").click();
			// Click CRM/SFA
			driver.findElementByLinkText("CRM/SFA").click();
			// Click Create Lead
			driver.findElementByLinkText("Create Lead").click();
			// Drop down
			WebElement src = driver.findElementById("createLeadForm_dataSourceId");
			Select dropDown = new Select(src);
			// Using VisibleText  
			//dropDown.selectByVisibleText("Conference");
			/*// Using index
			List<WebElement> options2 = dropDown.getOptions();
			dropDown.selectByIndex(options2.size() - 2);*/
			List<WebElement> options = dropDown.getOptions();
			for (WebElement eachOption : options) {
				System.out.println(eachOption.getText());
			}
			// Using Value
			//dropDown.selectByValue("");
		} catch (NoSuchElementException e) {
			System.out.println("NoSuchElementException Exception thrown");
			//throw new RuntimeException();
		}
		finally {
			// Close browser
			driver.close();
		}
		
	}
}
		
