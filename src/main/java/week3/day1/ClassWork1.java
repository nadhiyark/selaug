package week3.day1;



import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ClassWork1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		try
		{
			driver.manage().window().maximize();
			driver.get("http://leaftaps.com/opentaps");
			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Create Lead").click();
			//pls remove 1
			driver.findElementById("createLeadForm_companyName1").sendKeys("TestLeaf");
			driver.findElementById("createLeadForm_firstName").sendKeys("Nadhiya");
			driver.findElementById("createLeadForm_lastName").sendKeys("Kumaraswamy");
			WebElement src=	driver.findElementById("createLeadForm_dataSourceId");

			Select dropdown = new Select(src);

			dropdown.selectByVisibleText("Direct Mail");

			WebElement src1=driver.findElementById("createLeadForm_marketingCampaignId");

			Select dropdown1 = new Select(src1);
			List <WebElement> options = dropdown1.getOptions();

			dropdown1.selectByIndex(options.size()-2);
			//dropdown1.selectByIndex(2);
			driver.findElementByClassName("smallSubmit").click(); }
		catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//System.out.println();
			throw new RuntimeException();
			
		}

		finally {

			driver.close();
		}



	}
}
