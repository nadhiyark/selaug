package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTCT_Demo {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElementById("userRegistrationForm:userName").sendKeys("testleaf");
			driver.findElementById("userRegistrationForm:password").sendKeys("testleaf1");
			driver.findElementById("userRegistrationForm:confpasword").sendKeys("testleaf1");
			Select select = new Select(driver.findElementById("userRegistrationForm:securityQ"));
			select.selectByIndex(2);
			driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("air force school");
			driver.findElementById("userRegistrationForm:firstName").sendKeys("test");
			driver.findElementById("userRegistrationForm:gender:1").click();
			driver.findElementById("userRegistrationForm:maritalStatus:1").click();
			WebElement element =driver.findElementById("userRegistrationForm:occupation");
			selectDropdown(element,"Professional");
			WebElement element1 =driver.findElementById("userRegistrationForm:countries");
			selectDropdown(element1,"India");
			driver.findElementById("userRegistrationForm:email").sendKeys("abc@gmail.com");
			driver.findElementById("userRegistrationForm:mobile").sendKeys("12345678");
			WebElement element2 =driver.findElementById("userRegistrationForm:nationalityId");
			selectDropdown(element2,"India");
			driver.findElementById("userRegistrationForm:address").sendKeys("No 1, Velachery");
			driver.findElementById("userRegistrationForm:pincode").sendKeys("600042",Keys.TAB);
			Thread.sleep(2000);
			Select selectcity = new Select(driver.findElementById("userRegistrationForm:cityName"));
			List<WebElement> cities = selectcity.getOptions();
			System.out.println("Size"+cities.size() +cities.get(1).getText());
			selectcity.selectByVisibleText(cities.get(1).getText());
			Thread.sleep(2000);
			Select selectPostOffice = new Select(driver.findElementById("userRegistrationForm:postofficeName"));
			List<WebElement> postOffices = selectPostOffice.getOptions();
			System.out.println("Size"+postOffices.size() +postOffices.get(1).getText());
			selectPostOffice.selectByVisibleText(postOffices.get(1).getText());
			driver.findElementById("userRegistrationForm:landline").sendKeys("044 123456");
		}catch(StaleElementReferenceException | InterruptedException  e){
			e.printStackTrace();
		}
	}
	public static void selectDropdown(WebElement ele, String text) {
		Select select = new Select(ele);
		select.selectByVisibleText(text);
	}
	
}
