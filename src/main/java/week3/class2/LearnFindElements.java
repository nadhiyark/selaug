package week3.class2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnFindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		// Set binding
		// maximize the window
		driver.manage().window().maximize();
		// Set timeout
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// load URL
		driver.get("http://www.crystalcruises.com/cruises/calendar?year=2018");
		// Enter user name
		List<WebElement> allQuote = driver.findElementsByLinkText("REQUEST A QUOTE");
		System.out.println(allQuote.size());
		WebElement second = allQuote.get(1);

		second.click();
		

	}
}
		



