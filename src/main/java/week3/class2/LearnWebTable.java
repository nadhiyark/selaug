package week3.class2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnWebTable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// Set binding
		// maximize the window
		driver.manage().window().maximize();
		// Set timeout
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// load URL
		driver.get("https://erail.in");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("TBM",Keys.TAB);

		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("TSI",Keys.TAB);

		driver.findElementById("chkSelectDateOnly").click();
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> tableRow = table.findElements(By.tagName("tr"));
		System.out.println(tableRow.size());
		
		WebElement firstRow = tableRow.get(0);
		List<WebElement> tableColumn = firstRow.findElements(By.tagName("td"));
		System.out.println(tableColumn.size());
		String trainNum = tableColumn.get(0).getText();
		System.out.println(trainNum);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		Boolean until = wait.until(ExpectedConditions.textToBePresentInElement(tableColumn.get(0), "067"));
		
		
		/*Boolean until = wait.until(ExpectedConditions.textToBePresentInElement(tableColumn.get(0), "06027"));
		System.out.println(until);*/

	}
}