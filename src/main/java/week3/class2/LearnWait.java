package week3.class2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnWait {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			// Set timeout
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// load URL
			driver.get("http://leaftaps.com/opentaps");
			
				driver.findElementById("username")
				.sendKeys("DemoSalesManager", Keys.TAB);
			
			// Enter password
			driver.findElementById("password").sendKeys("crmsfa");
			// Click Login
			driver.findElementByClassName("decorativeSubmit").click();
			// Click CRM/SFA
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Leads").click();
			driver.findElementByLinkText("Find Leads").click();
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));*/
			driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
	}}