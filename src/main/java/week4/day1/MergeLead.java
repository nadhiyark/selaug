package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps");

		driver.findElementById("username")
		.sendKeys("DemoSalesManager", Keys.TAB);

		// Enter password
		driver.findElementById("password").sendKeys("crmsfa");
		// Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//table[@name='ComboBox_partyIdFrom']/following::img").click();

		Set<String> allwindows  = driver.getWindowHandles();
		List<String>listofwindows= new ArrayList<>();
		listofwindows.addAll(allwindows);

		driver.switchTo().window(listofwindows.get(1));
		driver.findElementByXPath("//input[@name='id']").sendKeys("10109");
		driver.findElementByXPath("//button[@class='x-btn-text']").click();

		/*	try
		{
		driver.findElementByLinkText("10089").click();
		}

		catch(Exception e)
		{

			System.out.println(e.getMessage());
		}
		 */

		driver.navigate().refresh();
		driver.findElementByLinkText("10109").click();

		//move to first window
		allwindows  = driver.getWindowHandles();
		listofwindows= new ArrayList<>();
		listofwindows.addAll(allwindows);
		driver.switchTo().window(listofwindows.get(0));

		driver.findElementByClassName("buttonDangerous").click();

		//Accept Aler

		Alert alert = driver.switchTo().alert();
		alert.accept();

		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//div[@class='x-form-element']/following::input)[32]").sendKeys("10109");
		driver.findElementByXPath("(//td[@class='x-panel-btn-td'])[6]").click();
		//driver.close();

	}
}
