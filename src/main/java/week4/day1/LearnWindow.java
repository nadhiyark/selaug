package week4.day1;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");		
		ChromeDriver driver= new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://www.irctc.co.in/nget/train-search");
		
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> windowsList=new ArrayList <>();
		windowsList.addAll(windowHandles);
		
		driver.switchTo().window(windowsList.get(1));
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getTitle());
		
		File snap = driver.getScreenshotAs(OutputType.FILE);
		File Path=new File("./SNAPS/Irctc.png");
		FileUtils.copyFile(snap, Path);
		
		windowHandles = driver.getWindowHandles();
		windowsList=new ArrayList <>();
		windowsList.addAll(windowHandles);
		
		
		
		

	}

}