package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alertframe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");

		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");

		driver.switchTo().frame("iframeResult");

		driver.findElementByXPath("//button[text()='Try it']").click();;

		Alert alert = driver.switchTo().alert();

		String text = alert.getText();
		System.out.println(text);


		alert.sendKeys("Nadhiya");
		alert.accept();


		WebElement output = driver.findElementById("demo");
		System.out.println(output.getText());
		String text2 = output.getText();
		
		if(text2.contains("Nadhiya"))
		{
			System.out.println("passed");
		}

		else
		{
			System.out.println("Failed");
		}
		
		
		driver.switchTo().defaultContent();




	}


}

