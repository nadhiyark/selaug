package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestCaseDeleteLead {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps");

		driver.findElementById("username")
		.sendKeys("DemoSalesManager", Keys.TAB);

		// Enter password
		driver.findElementById("password").sendKeys("crmsfa");
		// Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//span[text()='Phone']").click();
		//driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9");
		driver.findElementByXPath("(//button[text()='Find Leads'])").click();

		//capture grid results

		File src= driver.getScreenshotAs(OutputType.FILE);
		File destination =new File("./snaps/phone.png");
		FileUtils.copyFile(src, destination);
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		driver.findElementByXPath("//a[text()='Delete']").click();

		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Name and ID").click();
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input").sendKeys("10823");

	}

}
