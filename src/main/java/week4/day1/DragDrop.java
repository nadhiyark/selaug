package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/draggable/");
		
		Actions builder=new Actions(driver);
		WebElement frame = driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frame);
		WebElement drag = driver.findElementById("draggable");
		int y = drag.getLocation().getY();
		System.out.println(y);
		int x = drag.getLocation().getX();
		System.out.println(x);
		builder.dragAndDropBy(drag, x+100, y+100).perform();
		
		
		
		
		
		

	}

}