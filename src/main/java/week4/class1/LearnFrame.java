package week4.class1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		// Set binding
		// maximize the window
		driver.manage().window().maximize();
		// Set timeout
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// load URL
		driver.get("http://jqueryui.com/selectable/");
		// switch into frame
		driver.switchTo().frame(0);
		
		driver.findElementByXPath("//li[text()='Item 2']").click();
		// come out of frame
		driver.switchTo().defaultContent();
		driver.findElementByLinkText("Download").click();
		
	}
}

		
		
		
		
		
		
		
		
		
		
		
		
		