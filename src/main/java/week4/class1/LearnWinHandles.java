package week4.class1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWinHandles {
	
	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://legacy.crystalcruises.com/");
		driver.findElementByLinkText("GUEST CHECK-IN").click();		
		System.out.println(driver.getTitle());
		
		Set<String> allwindows = driver.getWindowHandles();	
		List<String> listOfWindows = new ArrayList<>();
		listOfWindows.addAll(allwindows);
		
		driver.switchTo().window(listOfWindows.get(1));
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img.png");
		FileUtils.copyFile(src, desc);
		
		
		
		
		
		driver.switchTo().window(listOfWindows.get(1));
		System.out.println(driver.getTitle());
		
		driver.findElementByPartialLinkText("View Offe").click();
		
		allwindows = driver.getWindowHandles();
		listOfWindows = new ArrayList<String>();
		listOfWindows.addAll(allwindows);
		
		driver.switchTo().window(listOfWindows.get(2));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		
		File src1 = driver.getScreenshotAs(OutputType.FILE);
		File desc1 = new File("./snaps/img.png");
		FileUtils.copyFile(src1, desc1);
		
	}
	
	
}