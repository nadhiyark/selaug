package week4.class1;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.mongodb.BulkUpdateRequestBuilder;

public class MoveEle {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// maximize the window
		driver.manage().window().maximize();
		// Set timeout
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// load URL
		driver.get("https://www.flipkart.com/");
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		
		Actions builder = new Actions(driver);
		WebElement elec = driver.findElementByXPath("//span[text()='Electronics']");
		WebElement apple = driver.findElementByLinkText("Apple");
		builder
		.moveToElement(elec).pause(2000)
		.click(apple).perform();		
		//.perform();
		
//		builder.dragAndDropBy(source, xOffset, yOffset)
		//Thread.sleep(3000);
	}
}