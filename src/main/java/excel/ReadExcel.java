package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	/*public static void main(String[] args) throws IOException {
		
		XSSFWorkbook wbook = new XSSFWorkbook("./TestData/CL.xlsx");
		XSSFSheet sheet = wbook.getSheet("CL");
	//	XSSFSheet sheet=wbook.getSheetAt(index);
		int rowCount = sheet.getLastRowNum();
		System.out.println("RowCount "+ rowCount);
		int ColumnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("ClolumnCount "+ ColumnCount);
		
		for(int j=1; j<=rowCount;j++)
		{
			XSSFRow row = sheet.getRow(j);
			for(int i=0;i<ColumnCount; i++ )
			{
				XSSFCell cell = row.getCell(i);
				
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);
			}
		}
		
		
		
		

	}
*/
	
	
public static Object[][] getExcelData(String fileName) throws IOException {
		
		XSSFWorkbook wbook = new XSSFWorkbook("./TestData/"+fileName+".xlsx");
		//XSSFSheet sheet = wbook.getSheet("CL");
		XSSFSheet sheet=wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println("RowCount "+ rowCount);
		int ColumnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("ClolumnCount "+ ColumnCount);
		
		Object[][] data = new Object[rowCount][ColumnCount];
		
		for(int j=1; j<=rowCount;j++)
		{
			XSSFRow row = sheet.getRow(j);
			for(int i=0;i<ColumnCount; i++ )
			{
				XSSFCell cell = row.getCell(i);
				
				data[j-1][i] = cell.getStringCellValue();
				
			}
		}
		return data;
		
		
		
		

	}
}
