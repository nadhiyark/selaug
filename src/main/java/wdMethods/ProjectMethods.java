package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import excel.ReadExcel;

public class ProjectMethods extends SeMethods_ReportGene{
	@BeforeSuite(groups= {"common"})
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups= {"common"})
	public void beforeClass() {
		startTestCase();
	}

	@BeforeMethod(groups= {"common"})
	//@Parameters({"url"})
	@Parameters({"url","username","password"})
	//public void login(String url) 
	public void login(String url, String username, String password )
	{
		
		
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
	
	}
	
	@AfterMethod(groups= {"common"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups= {"common"})
	public void afterSuite() {
		endResult();
	}
	
	
@DataProvider(name="positive")
	
	public Object[][] fetchData() throws IOException{
		return ReadExcel.getExcelData(fileName) ;
}
	
	
	
	
	
	
	
}
