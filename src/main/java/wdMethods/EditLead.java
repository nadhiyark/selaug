package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class EditLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "editLead";
		testCaseDesc= "editing a lead";
		category="Smoke";
		author="nadhiya";
	}
	@Test(dataProvider="positive")

	public void edit  (String compname) throws InterruptedException
	{		
		WebElement leads = locateElement("linkText", "Leads");
		click(leads);

		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);

		WebElement firstname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(firstname, "nadhiya");

		WebElement finleads = locateElement("xpath","//button[text()='Find Leads']");
		click(finleads);
		Thread.sleep(5000);

		WebElement firstele = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		click(firstele);
		boolean verifyTitle = verifyTitle("View Lead | opentaps CRM");
		System.out.println(verifyTitle);

		WebElement edit = locateElement("xpath","(//a[@class='subMenuButton'])[3]");
		click(edit);

		
		WebElement changecomp = locateElement("id", "updateLeadForm_companyName");
		
		//clear(changecomp);
		type(changecomp, compname);

		WebElement upadte = locateElement("xpath", "(//input[@name='submitButton'])[1]");
		click(upadte);
	
		WebElement verifycomp = locateElement("viewLead_companyName_sp");
		verifyPartialText(verifycomp, compname);
		
		closeBrowser();

	}
	@DataProvider(name="positive")
	public Object[][] fetchData() {
		Object[][] data = new Object[2][1];
		data[0][0]= "Igate"	;
		data[1][0]="capgemini";
		return data;
		}
	}
