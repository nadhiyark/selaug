package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DeleteLeads extends ProjectMethods{
	
	
	@BeforeTest(groups={"common"})
	public void setData() {
		testCaseName = "DelLead";
		testCaseDesc= "Deleting a lead";
		category="Smoke";
		author="nadhiya";
	}
	
		
	@Test(dataProvider="positive")

	public void delete(String pnumber) throws InterruptedException
	{
		
		WebElement leads = locateElement("linkText", "Leads");
		click(leads);

		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);

		WebElement phone = locateElement("xpath", "//span[text()='Phone']");
		click(phone);

		WebElement phonenumber = locateElement("xpath", "//input[@name='phoneNumber']");
	//	type(phonenumber, "8332896628");
		type(phonenumber,pnumber);
		

		WebElement finleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(finleads);

Thread.sleep(5000);
		WebElement firstrow = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String cleadid = getText(firstrow);
		//System.out.println(cleadid);
		click(firstrow);

		WebElement delete = locateElement("xpath", "//a[@class='subMenuButtonDangerous']");
		click(delete);

		findleads = locateElement("linkText", "Find Leads");
		click(findleads);
		
		Thread.sleep(5000);
		WebElement leaid = locateElement("xpath","(//label[text()='Lead ID:']/following::input)[1]");
		type(leaid, cleadid);
		
		
		 finleads = locateElement("xpath", "//button[text()='Find Leads']");
			click(finleads);


		WebElement error = locateElement("xpath", "//div[text()='No records to display']");
		
		
		verifyExactText(error, "No records to display");
		
		

	}
	@DataProvider(name="positive")
	public Object[][] fetchData()
	{
		Object[][] data= new Object[2][1];
		
		data[0][0]="9573259657";
		
		
		data[1][0]="8332896628";
		
		
		return data;
		
		
	}
	
	
}
