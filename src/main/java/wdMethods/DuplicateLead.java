package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DuplicateLead extends ProjectMethods {
	
	@BeforeTest(groups={"common"})
	public void setData() {
		testCaseName = "duplicatelead";
		testCaseDesc= "duplicTING  a lead";
		category="Smoke";
		author="nadhiya";
		
	}
	@Test(groups= {"regression"}, dataProvider="positive")
	
	
	public void duplicatelead(String emailid) throws InterruptedException {
		/*startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement usernameid = locateElement("username");
		type(usernameid, "DemoSalesManager");

		WebElement passwordid = locateElement("password");
		type(passwordid, "crmsfa");

		WebElement submit = locateElement("class", "decorativeSubmit");
		click(submit);

		WebElement crmsfa = locateElement("linkText", "CRM/SFA");
		click(crmsfa);*/
		WebElement leads = locateElement("linkText", "Leads");
		click(leads);

		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);
		WebElement email = locateElement("xpath", "//span[text()='Email']");
		click(email);

		WebElement emailaddress = locateElement("xpath", "//input[@name='emailAddress']");
		type(emailaddress, emailid);

		WebElement finleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(finleads);

		Thread.sleep(5000);
		WebElement firstelename = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		String firstelementtext = getText(firstelename);
		click(firstelename);

		WebElement duplicatelead = locateElement("linkText", "Duplicate Lead");
		click(duplicatelead);

		verifyTitle("Duplicate Lead");
		
		WebElement creatlead = locateElement("class", "smallSubmit");
		click(creatlead);
		
		WebElement dupname = locateElement("viewLead_firstName_sp");
		verifyPartialText(dupname, firstelementtext);
		

	}
	@DataProvider(name="positive")
	public Object[][] fetchData()
	{
		Object[][] data=new Object[2][1];
		data[0][0]="nadhiya@gmail.com";
		data[1][0]= "test@gmail.com";
		return data;
		
				
	}

}
