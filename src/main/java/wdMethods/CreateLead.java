package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ReadExcel;


public class CreateLead extends ProjectMethods{

	@BeforeTest(groups={"common"})
	public void setData() {
		testCaseName = "CreateLead";
		testCaseDesc= "Creating a lead";
		category="Smoke";
		author="nadhiya";
		fileName="CL";
	}
	@Test(groups={"smoke"},dataProvider="positive")//dataProvider="positive")
	
	public void createLead(String cName, String fName, String lName)
	{

		WebElement creatlead = locateElement("xpath", "//a[text()=\"Create Lead\"]");
		click(creatlead);

		WebElement companame = locateElement("xpath", "(//input[@name='companyName'])[2]");
		type(companame, cName);

		WebElement firstname = locateElement("createLeadForm_firstName");
		type(firstname, fName);

		WebElement lastname = locateElement("createLeadForm_lastName");
		type(lastname, lName);

		WebElement firstnamelocal = locateElement("createLeadForm_firstNameLocal");
		type(firstnamelocal, "nadhi");

		WebElement lastnamelocal = locateElement("createLeadForm_lastNameLocal");
		type(lastnamelocal, "rk");

		WebElement salutation = locateElement("createLeadForm_personalTitle");
		type(salutation, "Miss");

		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropdown(source, "index", "1");

		WebElement title = locateElement("id", "createLeadForm_generalProfTitle");
		type(title, "testingpractice");

		WebElement revenue = locateElement("id", "createLeadForm_annualRevenue");
		click(revenue);

		WebElement industry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropdown(industry, "visible", "Finance");


		WebElement ownership = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropdown(ownership, "value", "OWN_PUBLIC_CORP");

		WebElement sic = locateElement("id", "createLeadForm_sicCode");
		type(sic, "12345");

		WebElement description = locateElement("id", "createLeadForm_description");
		type(description, "test1");

		WebElement impnote = locateElement("id", "createLeadForm_importantNote");
		type(impnote, "test2");

		WebElement countrycode = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		type(countrycode, "12");

/*		WebElement areacode = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		type(areacode, "1");
*/
		WebElement extension = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(extension, "91");

		WebElement dept = locateElement("id", "createLeadForm_departmentName");
		type(dept, "IT");

		WebElement currency = locateElement("id", "createLeadForm_currencyUomId");
		selectDropdown(currency, "index", "3");

		WebElement noofemp = locateElement("id", "createLeadForm_numberEmployees");
		type(noofemp, "1234");

		WebElement ticker = locateElement("id", "createLeadForm_tickerSymbol");
		type(ticker, "test3");

		WebElement person = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(person, "test4");

		WebElement weburl = locateElement("id", "createLeadForm_primaryWebUrl");
		type(weburl, "www.hi.com");

		WebElement toname = locateElement("id", "createLeadForm_generalToName");
		type(toname, "nadhiii");

		WebElement adrsline1 = locateElement("id", "createLeadForm_generalAddress1");
		type(adrsline1, "34-1015");


		WebElement adrsline2 = locateElement("id", "createLeadForm_generalAddress2");
		type(adrsline2, "Bhajanakoil");

		WebElement city = locateElement("id", "createLeadForm_generalCity");
		type(city, "chitoor");

		WebElement state = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropdown(state, "visible", "Alaska");

		WebElement country = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropdown(country, "index", "2");

		WebElement zip = locateElement("id", "createLeadForm_generalPostalCode");
		type(zip, "517127");

		WebElement zipext = locateElement("id", "createLeadForm_generalPostalCode");
		type(zipext, "123");

		WebElement marketing = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropdown(marketing, "value", "9002");

		WebElement phone = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phone, "9573259657");

		WebElement email = locateElement("id", "createLeadForm_primaryEmail");
		type(email, "nadhiya@gmail.com");

		WebElement createlead = locateElement("xpath", "//input[@name=\"submitButton\"]");
		click(createlead);


		WebElement verifyname = locateElement("viewLead_firstName_sp");
		verifyExactText(verifyname, "Nadhiya");

	}
	
	/*@DataProvider(name="positive")
	public Object[][] fetchData()
	{
		Object[][] data= new Object[2][3];
		
		data[0][0]="testleaf";
		data[0][1]="nadhiya";
		data[0][2]="kumaraswamy";
		
		data[1][0]="Capgemini";
		data[1][1]="nadhi";
		data[1][2]="kumaraswamy";
		
		return data;
		
		
	}*/
	
	
	/*//In create lead tc data provider call getExcelData and execute the tc
	
	
	@DataProvider(name="positive")
	
	public Object[][] fetchData() throws IOException{
		return ReadExcel.getExcelData("CL") ;
		
	}*/
	
	}
	
	

