package InterviewPrograms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class ReverseGivenString {

	public static void main(String[] args) {
		 				
			
			String input = "Geeks for Geeks";	
			
			/*
			
			//Using built in reverse() method of the StringBuilder class
			  
	        StringBuilder input1 = new StringBuilder(); 
	  
	        // append a string into StringBuilder input1 
	        input1.append(input); 
	  
	        // reverse StringBuilder input1 
	        input1 = input1.reverse(); 
	  
	        // print reversed String 
	        System.out.println(input1); 
	    */
			
			
			 // convert String to character array 
	        // by using toCharArray 
			  
			/*
		          char[] try1 = input.toCharArray(); 
		  
		        for (int i = try1.length-1; i>=0; i--) 
		            System.out.print(try1[i]); */
			
			
			
			// Java program to Reverse a String using swapping 
			// of variables 
		
			/* char[] temparray = input.toCharArray(); 
		        int left, right=0; 
		        right = temparray.length-1; 
		  
		        for (left=0; left < right ; left++ ,right--) 
		        { 
		            // Swap values of left and right 
		            char temp = temparray[left]; 
		            temparray[left] = temparray[right]; 
		            temparray[right]=temp; 
		        } 
		  
		        for (char c : temparray) 
		            System.out.print(c); 
		        System.out.println(); */
		
			
			
			 char[] hello = input.toCharArray(); 
		        List<Character> trial1 = new ArrayList<>(); 
		  
		        for (char c: hello) 
		            trial1.add(c); 
		  
		        Collections.reverse(trial1);
		       // System.out.println(trial1);
		        
		        ListIterator li = trial1.listIterator(); 
		        while (li.hasNext()) 
		            System.out.print(li.next()); 

	}

}
