package InterviewPrograms;

import java.util.Scanner;

public class GivenNumberIsPrimeOrNot {
	
	public static void main(String args[])
	{
	int i, flag = 1; 
	  
    // Ask user for input 
  Scanner scan = new Scanner(System.in);
  System.out.println("Enter a number");
  int n=scan.nextInt();
 
  
    // Iterate from 2 to n/2 
    for (i = 2; i <= n / 2; i++) { 
  
        // If n is divisible by any number between 
        // 2 and n/2, it is not prime 
        if (n % i == 0) { 
            flag = 0; 
            break; 
        } 
    } 
  
    if (flag == 1) { 
        System.out.println(n+"is prime number"); 
    } 
    else { 
    	System.out.println(n+"is not a prime number"); 
    } 
  
    //return 0; 
}
}
