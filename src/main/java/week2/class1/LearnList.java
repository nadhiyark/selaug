package week2.class1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {
	
	public static void main(String[] args) {

		//Single Dimension
		//Allow Duplicate
		//always in Ordered
		List<String> myPhones = new ArrayList<String>();
		myPhones.add("Nokia");
		myPhones.add("Samsung");
		myPhones.add("IPhone");
		myPhones.add("Samsung");
		myPhones.add(1, "moto");
		//myPhones.remove("IPhone");
		//myPhones.remove("Samsung");
		boolean contains = myPhones.contains("LG");
		System.out.println("LG contains "+contains);
		int size = myPhones.size();
		System.out.println("The Count of Mobiles "+size);
		Collections.sort(myPhones);
		System.out.println(myPhones);
		Collections.reverse(myPhones);
		for (String eachPhone : myPhones) {
			System.out.println(eachPhone);
		}
		String firstPhone = myPhones.get(0);
		System.out.println("The first Phone is :"+firstPhone);
		System.out.println("The last Phone is :"+myPhones.get(size-1));
		
		
		
		
		
		
		
		
		
		
	}


}
