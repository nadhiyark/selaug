package week2.class1;

public interface MobileDesign extends TeleComDesign{

	public void support4G();
	
	public void enableWifi();
	
	public void support5G();
	
	
}