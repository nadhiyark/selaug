package week2.class2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LearnRegex {

	public static void main(String[] args) {
	String text = "ABCDH1234K";
	String pat = "[A-Z]{5}\\d{4}[A-Z]";
	Pattern p = Pattern.compile(pat);
	Matcher m = p.matcher(text);
	System.out.println(m.matches());
	}
}


