package week2.class2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class LearnSet {	
	//Set single dimensional interface	
	//Implementation classes	
		/*HashSet - Random Order
		  TreeSet - Ascii Order
		  LinkedHashSet - Maintain(inserting) the Order*/
		
	public void learnSet() {		
		Set<String> mobiles = new LinkedHashSet<String>();
		mobiles.add("Nokia");
		mobiles.add("Moto");
		boolean add = mobiles.add("LG");
		System.out.println(add);
		mobiles.add("I-phone");
		boolean add2 = mobiles.add("Samsung");	
		System.out.println(add2);
		
		int size = mobiles.size();
		System.out.println(size);
		
		for (String eachMobile : mobiles) {			
			System.out.println(eachMobile);
		}
		
		List<String> lsd = new ArrayList<String>();
		lsd.addAll(mobiles);
		String string = lsd.get(size-2);
		System.out.println(string);
		
		
	}
	
	
	public void learnSet1() {		
		Set<Integer> mobiles = new LinkedHashSet<Integer>();
		mobiles.add(1);
		mobiles.add(12);
		boolean add = mobiles.add(28);
		System.out.println(add);
		mobiles.add(15);
		boolean add2 = mobiles.add(19);	
		System.out.println(add2);
		
		int size = mobiles.size();
		System.out.println(size);
		
		for (Integer eachMobile : mobiles) {			
			System.out.println(eachMobile);
		}
		
		List<Integer> lsd = new ArrayList<Integer>();
		lsd.addAll(mobiles);
		Integer string = lsd.get(size-2);
		System.out.println(string);
		
		
	}
}