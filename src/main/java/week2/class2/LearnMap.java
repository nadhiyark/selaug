package week2.class2;

import java.util.HashMap;
import java.util.Map;

public class LearnMap {

	//Map two Dimension -> Key - Value
	/*HashMap
		TreeMap
		LinkedHashMap*/

	public static void main(String[] args) {

		Map<String, Integer> phones = new HashMap<String, Integer>();

		phones.put("Nokia", 900308835);
		phones.put("Moto", 900308835);
		phones.put("Samsung", 959770456);
		phones.put("Iphone", 959770456);
		
		int size = phones.size();
		System.out.println(size);
		
		phones.remove("Iphone");
		
		size = phones.size();
		System.out.println(size);
		
		boolean cKey = phones.containsKey("Samsung");
		System.out.println(cKey);
		
		




	}

}