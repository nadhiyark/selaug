package week2.class2;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;

public class HomeWork {
	
	String txt = "Amazon India";
	
	public void sortChar() {		
		char[] ch = txt.toCharArray();		
		List<Character> lst = new ArrayList<Character>();		
		for (Character eachChar : ch) {
			lst.add(eachChar);
		}				
		//System.out.println(lst);
		Collections.sort(lst);
		System.out.println(lst);		
	}
	
	public void reverseChar() {		
		char[] ch = txt.toLowerCase().toCharArray();		
		List<Character> lst = new ArrayList<Character>();		
		for (Character eachChar : ch) {
			lst.add(eachChar);
		}				
		//System.out.println(lst);
		Collections.sort(lst);		
		Collections.reverse(lst);		
		System.out.println(lst);		
	}
	
	public void removeDups() {	
		
		char[] ch = txt.toLowerCase().toCharArray();		
		List<Character> lst = new ArrayList<Character>();		
		for (Character eachChar : ch) {
			
			if (!lst.contains(eachChar)) {
				lst.add(eachChar);				
			}			
		}				
		//System.out.println(lst);
		Collections.sort(lst);
		System.out.println(lst);		
	}
	

	public static void main(String[] args) {
		HomeWork hw = new HomeWork();
		hw.removeDups();

	}
}