package week2.class2;

import java.util.HashMap;
import java.util.Map;

public class EachCharacterCountMAP {
	
	//Each Character count

	public static void main(String[] args) {
		
		String txt = "Amazon India";
		char[] ch = txt.toCharArray();
		
		Map<Character, Integer> map = new HashMap<Character, Integer>();
				
		for (char c : ch) {		
			if (map.containsKey(c)) {				
				Integer val = map.get(c)+1;
				map.put(c, val);				
			}else {
				map.put(c, 1);
			}						
		}
		
		System.out.println(map);
		

	}

}
