package week2.class2;


public class LearnStatic {

	static int num1 =10;
	int num2 =10;
	public static void main(String[] args) {
		LearnStatic ls = new LearnStatic();
		ls.counter();
		ls.counter();
		LearnStatic ls1 = new LearnStatic();
		ls1.counter();
		ls1.counter();
		ls.counter();
	}
	public void counter() {
		num1++;
		num2++;
		System.out.println("Static number = "+num1);
		System.out.println("Non-static number = "+num2);
	}
	
}
