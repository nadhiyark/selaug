package week2.day2;

import java.util.Scanner;

public class Aug31Task1 {

	public static void main(String[] args) {
		// WAP to findout given string is palindrome or not
		Scanner scan =new Scanner(System.in);
		System.out.println("enter string");
		String original = scan.nextLine();
		int len= original.length();
		String reverse="";
		for(int i=len-1; i>=0; i--)
		{
			reverse = reverse+original.charAt(i);
		}
		if(original.equalsIgnoreCase(reverse))
		{
			System.out.println("given is palindrome");
		}
		else
		{
			System.out.println("Given is not a palindrome");
		}

	}

}
