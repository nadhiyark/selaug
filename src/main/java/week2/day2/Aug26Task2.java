package week2.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Aug26Task2 {
	//This program would find out the duplicate characters in a String and would display the count of them.

		 
	    public void findDuplicateChars(String str){
	    	
	    	//Create a HashMap 
	         
	        Map<Character, Integer> map = new HashMap<Character, Integer>(); 
	        
	      //Convert the String to char array
	        char[] chrs = str.toCharArray();
	        
	       /* logic: char are inserted as keys and their count
	         as values. If map contains the char already then
	         increase the value by 1
	        */
	        for(Character ch:chrs){
	            if(map.containsKey(ch)){
	                map.put(ch, map.get(ch)+1);
	            } else {
	                map.put(ch, 1);
	            }
	        }
	        
	      //Obtaining set of keys
	        Set<Character> keys = map.keySet();
	        for(Character ch:keys){
	            if(map.get(ch) > 1){
	                System.out.println(ch+"--->"+map.get(ch));
	            }
	        }
	    }
	     
	    public static void main(String a[]){
	    	Aug26Task2 dcs = new Aug26Task2();
	        dcs.findDuplicateChars("nnaadhhiyya rr kk");
	    }
	}
