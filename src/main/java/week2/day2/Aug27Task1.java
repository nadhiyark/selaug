package week2.day2;

import java.util.Scanner;

public class Aug27Task1 {

	public static void main(String[] args) {
		// WAP to to display first n prime numbers
		//sample i/p : enter the value 30
		//o/p: 2 3 5 7 11 13 17 19 23 29
				
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter n value: ");
		int n = scan.nextInt();
		//int count =0;
		for(int i=1;i<=n; i++)
		{
			int count =0;
			for(int j=1;j<=i;j++)
			{
				if(i%j==0)
				{
				count = count+1;
				}
			}
			
			if(count==2)
			{
				System.out.println(i);
			}
		}
		}
		
	}


