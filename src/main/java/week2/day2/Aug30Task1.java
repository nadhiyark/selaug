package week2.day2;

import java.util.Scanner;

public class Aug30Task1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a :");
		int a = scan.nextInt();
		System.out.println("Enter b :");
		int b = scan.nextInt();
		
		for (int i=a; i<=b; i++)
		{
			
		
			 if(i%3==0 && i%5!=0)
			{
				String s = "FIZZ";
				System.out.print(s+" ");
				continue;
				
			}
			else if(i%5==0 && i%3!=0)
			{
				String s = "BUZZ";
				System.out.print(s+" ");
				continue;
				
			}
			else if(i%3==0 && i%5==0)
			{
				String s = "FIZZBUZZ";
				System.out.print(s+ " ");
				continue;
				
			}
			
			System.out.print(i + " ");
			
		}
		
	}

}
