package week2.day2;

import java.util.Scanner;

public class Aug28Task1 {

	public static void main(String[] args) {
		// WAP to print the sum of numbers in the array
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the range of array");
		int n= scan.nextInt();
		
		int numbers[] = new int[n];
		
		System.out.println("Enter elements in the array");
		
		for(int i=0; i<n; i++)
		{
			numbers[i]= scan.nextInt();
			
		}
		int sum=0;
		for(int i=0; i<n;i++)
		{
			sum=sum+numbers[i];
		}
		
		System.out.println("Sum of numbers in the array is "+sum);

	}

}
