package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Aug28Task2 {

	public static void main(String[] args) {
		// WAP to print the sum of numbers in the arrayList
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the range of array");
		int n= scan.nextInt();
		
		int numbers[] = new int[n];
		
		System.out.println("Enter elements in the array");
		
		for(int i=0; i<n; i++)
		{
			numbers[i]= scan.nextInt();
			
		}
		
		List<Integer> numbers1= new ArrayList<Integer>();
		
		for(Integer n1: numbers)
			numbers1.add(n1);
		
		int sum=0;
		
		for(int i=0; i<numbers1.size();i++)
		{
			sum=sum+numbers1.get(i);
		}
		
		System.out.println("Sum of numbers in the array is "+sum);

	}

}
