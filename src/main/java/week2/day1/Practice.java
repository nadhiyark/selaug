package week2.day1;

	import java.util.ArrayList;
	import java.util.Collections;
	import java.util.HashSet;
	import java.util.List;
	import java.util.Set;
	public class Practice {
	//Program to print in the sorted order of the input AmazonIndia
	//print character in reverse sorted order without case sensitive
	//remove duplicate and print in sorted order

	public class SortList 
	{
		public void main(String[] args) 
		{
			String name ="AmazonIndia";
			List<Character> eChar = new ArrayList<Character>();
			char []allChar=name.toCharArray();
			
			for (char c : allChar) 
			{
				eChar.add(c);
			}
			
			Collections.sort(eChar);
			System.out.println("printing the sorted order");
			for (char sort : eChar) 
			{
				System.out.println(sort);
			}
			eChar.clear();
			String uCase=name.toUpperCase();
			char []uChar=uCase.toCharArray();
			
			for (char d : uChar) 
			{
				eChar.add(d);
			}
			Collections.sort(eChar);
			Collections.reverse(eChar);
			System.out.println("printing the sorted reverse order");
			for (char rev : eChar) 
			{
				System.out.println(rev);
			}
			//to remove duplicate
			Set<Character> dup = new HashSet<>();
			dup.addAll(eChar);
			eChar.clear();
			eChar.addAll(dup);
			Collections.sort(eChar);
			System.out.println("Removed duplicates and printing in sorted order");
			for (Character g : eChar)
			{
				System.out.println(g);
			}
		}

	}

	}
