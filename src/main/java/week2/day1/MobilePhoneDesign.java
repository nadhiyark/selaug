package week2.day1;

public interface MobilePhoneDesign {
	
	public void wifiSupport();
	public void sim();

}
