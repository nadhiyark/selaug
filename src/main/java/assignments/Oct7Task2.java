package assignments;

import java.util.Scanner;

public class Oct7Task2 {

	public static void main(String[] args) {
		// Findout count of given character in a string
				// i/p: Amazon India
		//char: a
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a string");
		String str = scan.nextLine();
		//System.out.println(str);
		char character='a';
		
		char[] charArray = str.toCharArray();
		//System.out.println(charArray);
		int length = charArray.length;
		//System.out.println(length);
		
		int count=0;
		for(int i=0; i<length;i++)
		{
			if(charArray[i]==character)
			{
				count=count+1;
			}
		}
		System.out.println(count);

	}

}
