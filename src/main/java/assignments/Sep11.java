package assignments;

import java.util.Scanner;

public class Sep11 {

	public static void main(String[] args) {
		/* Write a java program to find the percentage of 
		 * uppercase letters, lowercase letters, digits and other special characters(including space) 
		 * in the given string.
		 * For Ex-��Tiger Runs @ The Speed Of 100 km/hour.�,
		 * Number of uppercase letters is�5. So percentage is�13.16%
		 * Number of lowercase�letters is�20. So percentage is�52.63%
		 * Number of digits�is�3. So percentage is�7.89%
		 * Number of other�characters�is�10. So percentage is�26.32%*/
		
		Scanner input = new Scanner(System.in);  
		System.out.print("Please enter a Password: ");  
		String password1 = input.nextLine();  
		
		int n = password1.length();
		int upper, lower,digits, splchar;
		upper= lower=digits= splchar=0;
		char c;
		
		for(int i=0; i<n;i++)
		{
		 c= password1.charAt(i);
		 
		 if(Character.isUpperCase(c))
			 upper++;
		 else if(Character.isLowerCase(c))
			 lower++;
		 else if(Character.isDigit(c))
			 digits++;
		 else
			 splchar++;
	 
	}
		float f1,f2,f3,f4;
		f1=((upper*100)/n);
		f2=((lower*100)/n);
		f3=((digits*100)/n);
		f4=((splchar*100)/n);
		System.out.println("Number of uppercase letters is�"+upper+". So percentage is�"+f1+"%");
		System.out.println("Number of lowercase�letters is�"+lower+". So percentage is�"+f2+"%");
		System.out.println("Number of digits�is�"+digits+". So percentage is�"+f3+"%");
		System.out.println("Number of other�characters�is�"+splchar+". So percentage is�"+f4+"%");

	}}
